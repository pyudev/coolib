local M = {
    str = "abc",
    num = 123,
    bool = false
}

function M:new(str, num, bool)
    setmetatable(M, self)
    self.str = str
    self.num = num
    self.bool = bool
    return self
end

function M:tostr()
    return self.str .. ";" .. tostring(self.num) .. ";" .. tostring(self.bool)
end

return M